import os
import unittest
from ranqx_util_files import HTMLTestRunner
from ranqx_testcases.login_existing_users_test_cases import RanqxLoginExistingUsersTestCases
from ranqx_testcases.signup_user_test_cases import SignupUsersTestCases
from ranqx_testcases.bank_super_admin_test_cases import BankSuperAdminTestCases

# get the directory path to output report file
dir = os.getcwd()

# get all tests from test cases
#existing_users_login_scripts = unittest.TestLoader().loadTestsFromTestCase(RanqxLoginExistingUsersTestCases)
signup_test_cases = unittest.TestLoader().loadTestsFromTestCase(SignupUsersTestCases)
#bank_super_admin_cases = unittest.TestLoader().loadTestsFromTestCase(BankSuperAdminTestCases)
#unittest.TextTestRunner(verbosity=2).run(existing_users_login_scripts)
# create a test suite combining all tests
Regression_tests = unittest.TestSuite(signup_test_cases)
#unittest.TestSuite([existing_users_login_scripts, signup_test_cases])

# open the report file
outfile = open(dir + "/RanqxTestReport.html", "w")

# configure HTMLTestRunner options
runner = HTMLTestRunner.HTMLTestRunner(
                 stream=outfile,
                 title='',
                 description=''
                 )

# run the suite using HTMLTestRunner
runner.run(Regression_tests)